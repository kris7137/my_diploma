from datetime import datetime
from django.db import models


class Point(models.Model):
    def __str__(self):
        return "{}".format(self.point_address)
    point_address = models.CharField(max_length=150)
    phone_one = models.CharField(max_length=20)
    phone_two = models.CharField(max_length=20)


class Card(models.Model):
    def __str__(self):
        return "Скидка {} %".format(self.discount)
    discount = models.IntegerField(default=0)


class Customer(models.Model):
    user_name = models.CharField(max_length=50)
    user_phone = models.CharField(max_length=50)
    user_address = models.CharField(max_length=150)
    card = models.ForeignKey(Card, on_delete=models.CASCADE)


class Deliveryman(models.Model):
    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    del_phone = models.CharField(max_length=20)
    active = models.IntegerField(default=0)


class Order(models.Model):
    def __str__(self):
        return "№{}".format(self.id)
    date_created = models.DateTimeField(default=datetime.now, blank=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    point = models.ForeignKey(Point, on_delete=models.CASCADE)
    sum_order = models.IntegerField(default=0)
    status = models.IntegerField(default=0)
    deliveryman = models.ForeignKey(Deliveryman, on_delete=models.CASCADE)
    comment = models.CharField(max_length=500)


class Dish(models.Model):
    def __str__(self):
        return "{}".format(self.name)
    name = models.CharField(max_length=50)
    price = models.IntegerField(default=0)
    picture = models.CharField(max_length=255)
    description = models.CharField(max_length=400)
    type_dish = models.CharField(max_length=20)
    subtype_dish = models.CharField(max_length=20)


class Dishes_in_order(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    dish = models.ForeignKey(Dish, on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)
