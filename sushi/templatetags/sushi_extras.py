from django import template
from sushi.constants import order_status_to_string

register = template.Library()

register.filter('order_status_to_string', order_status_to_string)
