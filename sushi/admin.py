from django.contrib import admin

from .models import Point, Card, Customer, Deliveryman, Order, \
    Dish, Dishes_in_order


admin.site.register(Point)
admin.site.register(Card)
admin.site.register(Customer)
admin.site.register(Deliveryman)
admin.site.register(Order)
admin.site.register(Dish)
admin.site.register(Dishes_in_order)
