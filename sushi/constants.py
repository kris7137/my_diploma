ORDER_NEW = 0
ORDER_CONFIRMED = 1
ORDER_DELIVERY = 2
ORDER_CLOSED = 3
ORDER_CANCELLED = 4  # Отмененный заказ


def order_status_to_string(status):
    order_status_strings = [
        "Новый",
        "Подтвержденный",
        "Доставляется",
        "Доставлен",
        "Отменен"
    ]
    return order_status_strings[status]
