from django.http import HttpResponse
from django.template import loader, RequestContext
from .models import *
from .constants import *


def index(request):
    template = loader.get_template('sushi/index.html')
    return HttpResponse(template.render())


def delivery_zone(request):
    template = loader.get_template('sushi/delivery_zone.html')
    return HttpResponse(template.render())


def promo(request):
    template = loader.get_template('sushi/promo.html')
    return HttpResponse(template.render())


def cards(request):
    template = loader.get_template('sushi/cards.html')
    return HttpResponse(template.render())


def contact(request):
    template = loader.get_template('sushi/contact.html')
    return HttpResponse(template.render())


def split_list(data, split_factor):
    data_divided = []
    local_items = []
    i = 0
    for item in data:
        local_items.append(item)
        i += 1
        if i == split_factor:
            data_divided.append(local_items)
            local_items = []
            i = 0

    if len(local_items) != 0:
        data_divided.append(local_items)

    return data_divided


def get_menu(request, type_dish):
    template = loader.get_template('sushi/menu.html')
    dishes = Dish.objects.filter(type_dish=type_dish)

    split_factor = 4
    dishes_divided = split_list(dishes, split_factor)

    context = RequestContext(request, {
        'type_page': type_dish,
        'dishes_divided': dishes_divided,
    })
    return HttpResponse(template.render(context))


def backend_delivery(request):
    template = loader.get_template('sushi/backend/delivery.html')
    orders = Order.objects.exclude(status=ORDER_CLOSED) \
        .exclude(status=ORDER_CANCELLED)

    # for order in orders:
    #     order["status"] = order_status_to_string(order["status"])

    context = RequestContext(request, {
        'orders': orders,
    })
    return HttpResponse(template.render(context))


def korzina(request):
    template = loader.get_template('sushi/korzina.html')
    return HttpResponse(template.render())
