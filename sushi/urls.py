from django.conf.urls import url

from . import views

app_name = 'sushi'
urlpatterns = [
    url(r'^$', views.index),
    url(r'^delivery_zone/$', views.delivery_zone),
    url(r'^promo/$', views.promo),
    url(r'^cards/$', views.cards),
    url(r'^contact/$', views.contact),
    url(r'^korzina/$', views.korzina),

    url(r'^menu/rolls/$', views.get_menu, {'type_dish': 'Роллы'}),
    url(r'^menu/sushi/$', views.get_menu, {'type_dish': 'Суши'}),
    url(r'^menu/soup/$', views.get_menu, {'type_dish': 'Супы'}),
    url(r'^menu/sashimi/$', views.get_menu, {'type_dish': 'Сашими'}),
    url(r'^menu/drink/$', views.get_menu, {'type_dish': 'Напитки'}),
    url(r'^menu/salad/$', views.get_menu, {'type_dish': 'Салаты'}),
    url(r'^menu/dessert/$', views.get_menu, {'type_dish': 'Десерты'}),
    url(r'^menu/sets/$', views.get_menu, {'type_dish': 'Сеты'}),
    url(r'^menu/hot_dishes/$', views.get_menu,
        {'type_dish': 'Горячие блюда'}),

    url(r'^backend/delivery/$', views.backend_delivery),
]
